<?php

declare(strict_types=1);

namespace App\Application\User\GetUsers;

final class GetUsersRequest
{
    public function __construct(
        private ?string $activationLength = null,
        private ?array $countries = null,
    ) { }

    public static function fromRequest(string $activationLength, string $countries): GetUsersRequest
    {
        return new self(
            empty($activationLength) ? null : $activationLength,
            empty($countries) ? null : explode(',', $countries)
        );
    }

    public function getActivationLength(): ?string
    {
        return $this->activationLength;
    }

    public function getCountries(): ?array
    {
        return $this->countries;
    }
}
