<?php

declare(strict_types=1);

namespace App\Application\User\GetUsers;

use App\Domain\Common\CountryCodeCollection;
use App\Domain\User\UserRestrictions;
use App\Domain\User\UserRepository;

final class GetUsers
{
    public function __construct(private UserRepository $userRepository) { }

    public function execute(GetUsersRequest $request): GetUsersResponse
    {
        $this->guardValidActivationLength($request);

        return new GetUsersResponse($this->userRepository->all(
            new UserRestrictions(
                $this->activationLengthOrNull($request),
                $this->countriesOrNull($request)
            )
        ));
    }

    private function activationLengthOrNull(GetUsersRequest $request) : ?int
    {
        return null === $request->getActivationLength() ?
            null :
            (int) $request->getActivationLength();
    }

    private function countriesOrNull(GetUsersRequest $request): ? CountryCodeCollection
    {
        return null === $request->getCountries() ?
            null :
            CountryCodeCollection::fromValues($request->getCountries());
    }

    private function guardValidActivationLength(GetUsersRequest $request): void
    {
        if (null !== $request->getActivationLength() && !is_numeric($request->getActivationLength())) {
            throw new \InvalidArgumentException('activation length must be numeric');
        }
    }
}
