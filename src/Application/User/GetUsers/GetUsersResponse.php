<?php

declare(strict_types=1);

namespace App\Application\User\GetUsers;

use App\Domain\User\User;

final class GetUsersResponse implements \JsonSerializable
{
    const DEFAULT_DATE_FORMAT = 'Ymd';

    public function __construct(private array $users) { }

    public function jsonSerialize(): array
    {
        return [
            'items' => array_map(fn(User $user) : array => [
                'id' => $user->getId(),
                'name' => $user->getFirstName()->getValue(),
                'surname' => $user->getLastName()->getValue(),
                'email' => $user->getEmail()->getValue(),
                'country' => $user->getCountryCode()->getCode(),
                'createdAt' => $user->getCreatedAt()->format(self::DEFAULT_DATE_FORMAT),
                'activatedAt' => $user->getActivatedAt()->format(self::DEFAULT_DATE_FORMAT),
                'chargerId' => $user->getChargerId()
            ], $this->users)
        ];
    }
}
