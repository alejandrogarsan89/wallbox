<?php

declare(strict_types=1);

namespace App\Infrastructure\Csv;

final class CsvReader
{
    const CSV_DEFAULT_SEPARATOR = ',';
    const F_OPEN_MODE = 'r';

    public function getCsvRows(string $csvPath, string $separator = self::CSV_DEFAULT_SEPARATOR): array
    {
        $data = [];

        if (($stream = fopen($csvPath, self::F_OPEN_MODE)) == FALSE) {
            return [];
        }

        while (($row = fgetcsv(stream: $stream, length: 1000, separator: $separator)) !== FALSE)
        {
            $data[] = $row;
        }

        return $data;
    }
}
