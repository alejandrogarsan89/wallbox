<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Controller\User;

use App\Application\User\GetUsers\GetUsers;
use App\Application\User\GetUsers\GetUsersRequest;
use League\ISO3166\Exception\DomainException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

final class GetUsersController
{
    public function __construct(private GetUsers $getUsers) { }

    public function __invoke(Request $request): JsonResponse
    {
        try {
            return new JsonResponse($this->getUsers->execute(GetUsersRequest::fromRequest(
                $request->get('activation_length', ''),
                $request->get('countries', '')
            )));
        } catch (DomainException $exception) {
            return new JsonResponse(['message' => $exception->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
        } catch (\InvalidArgumentException $exception) {
            return new JsonResponse(['message' => $exception->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
        } catch (\Throwable $exception) {
            return new JsonResponse(['message' => $exception->getMessage(), 'type' => get_class($exception)], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
