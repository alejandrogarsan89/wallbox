<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\User;

use App\Domain\User\User;

final class CsvUserAdapter
{
    const CSV_ROW_USER_ID_INDEX = 0;
    const CSV_ROW_USER_FIRSTNAME_INDEX = 1;
    const CSV_ROW_USER_LASTNAME_INDEX = 2;
    const CSV_ROW_USER_EMAIL_INDEX = 3;
    const CSV_ROW_USER_COUNTRY_INDEX = 4;
    const CSV_ROW_USER_CREATED_AT_INDEX = 5;
    const CSV_ROW_USER_ACTIVATED_AT_INDEX = 6;
    const CSV_ROW_USER_CHARGER_CODE_INDEX = 7;

    public function adaptMany(array $userCsvRows): array
    {
        return array_map(fn(array $csvRow): User => $this->adapt($csvRow), $userCsvRows);
    }

    private function adapt(array $userCsvRow): User
    {
        return User::create(
            id: $userCsvRow[self::CSV_ROW_USER_ID_INDEX],
            firstName:$userCsvRow[self::CSV_ROW_USER_FIRSTNAME_INDEX],
            lastName: $userCsvRow[self::CSV_ROW_USER_LASTNAME_INDEX],
            email: $userCsvRow[self::CSV_ROW_USER_EMAIL_INDEX],
            countryCode:$userCsvRow[self::CSV_ROW_USER_COUNTRY_INDEX],
            createdAt: $userCsvRow[self::CSV_ROW_USER_CREATED_AT_INDEX],
            activatedAt: $userCsvRow[self::CSV_ROW_USER_ACTIVATED_AT_INDEX],
            chargerId: $userCsvRow[self::CSV_ROW_USER_CHARGER_CODE_INDEX]
        );
    }
}
