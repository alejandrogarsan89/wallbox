<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\User;

use App\Domain\User\UserFilter;
use App\Domain\User\UserRestrictions;
use App\Domain\User\UserRepository;
use App\Domain\User\UserSort;
use App\Infrastructure\Csv\CsvReader;

final class CsvUserRepository implements UserRepository
{
    const CSV_DEFAULT_SEPARATOR = ',';

    public function __construct(
        private CsvReader $csvReader,
        private CsvUserAdapter $csvUserAdapter,
        private UserSort $userSort,
        private UserFilter $userFilter,
        private string $csvPath = '/users.csv'
    ) { }

    public function all(UserRestrictions $restrictions): array
    {
        $csvRows = $this->csvReader->getCsvRows(__DIR__ . $this->csvPath, self::CSV_DEFAULT_SEPARATOR);
        $users = $this->csvUserAdapter->adaptMany($csvRows);
        $users = $this->userFilter->filterByRestrictions($users, $restrictions);
        $users = $this->userSort->sort($users);

        return $users;
    }
}
