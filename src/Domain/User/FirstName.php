<?php

declare(strict_types=1);

namespace App\Domain\User;

final class FirstName
{
    const MAX_LENGTH_THRESHOLD = 30;

    public function __construct(private string $value)
    {
        $this->guardNotEmpty($value);
        $this->guardValidMaxLength($value);
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function equals(FirstName $firstName)
    {
        return $this->value === $firstName->getValue();
    }

    private function guardNotEmpty(string $value): void
    {
        if (empty($value)) {
            throw new EmptyValueException("firstname can not be empty");
        }
    }

    private function guardValidMaxLength(string $value): void
    {
        if (strlen($value) > self::MAX_LENGTH_THRESHOLD) {
            throw new ValueExceedsAllowedLimitException("first name max length exceeded");
        }
    }
}
