<?php

declare(strict_types=1);

namespace App\Domain\User;

final class EmptyValueException extends \InvalidArgumentException
{

}
