<?php

declare(strict_types=1);

namespace App\Domain\User;

final class DefaultUserSortStrategy implements UserSortStrategy
{
    public function compare(User $userA, User $userB): int
    {
        return $userA->getFirstName()->equals($userB->getFirstName()) ?
            $userA->getLastName()->getValue() <=> $userB->getLastName()->getValue() :
            $userA->getFirstName()->getValue() <=> $userB->getFirstName()->getValue();
    }
}
