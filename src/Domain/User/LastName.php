<?php

declare(strict_types=1);

namespace App\Domain\User;

final class LastName
{
    const MAX_LENGTH_THRESHOLD = 30;

    public function __construct(private string $value)
    {
        $this->guardNotEmpty($value);
        $this->guardValidMaxLength($value);
    }

    public function getValue(): string
    {
        return $this->value;
    }

    private function guardNotEmpty(string $value): void
    {
        if (empty($value)) {
            throw new EmptyValueException("lastname can not be empty");
        }
    }

    private function guardValidMaxLength(string $value): void
    {
        if (strlen($value) > self::MAX_LENGTH_THRESHOLD) {
            throw new ValueExceedsAllowedLimitException("lastname max length exceeded");
        }
    }
}
