<?php

declare(strict_types=1);

namespace App\Domain\User;

use App\Domain\Common\CountryCode;
use App\Domain\Common\CountryCodeCollection;

final class UserRestrictions
{
    public function __construct(
        private ?int $activationLength = null,
        private ?CountryCodeCollection $countries = null,
    ){ }

    public function requiresRestrictionByActivationLength(): bool
    {
        return !is_null($this->activationLength);
    }

    public function requiresRestrictionByCountries(): bool
    {
        return !is_null($this->countries);
    }

    public function getActivationLength(): ?int
    {
        return $this->activationLength;
    }

    /**
     * @return CountryCode[] | null
     */
    public function getCountriesCodes(): ?CountryCodeCollection
    {
        return $this->countries;
    }
}
