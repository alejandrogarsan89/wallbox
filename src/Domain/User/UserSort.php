<?php

declare(strict_types=1);

namespace App\Domain\User;

final class UserSort
{
    public function __construct(private UserSortStrategy $userSortStrategy) { }

    public function sort(array $users): array
    {
        usort($users,
            fn(User $userA, User $userB): int => $this->userSortStrategy->compare($userA, $userB)
        );

        return $users;
    }
}
