<?php

declare(strict_types=1);

namespace App\Domain\User;

use App\Domain\Common\CountryCodeCollection;

final class UserFilter
{
    public function filterByRestrictions(array $users, UserRestrictions $restrictions): array
    {
        if ($restrictions->requiresRestrictionByActivationLength()) {
            $users = $this->filterByActivationLength($users, $restrictions->getActivationLength());
        }

        if ($restrictions->requiresRestrictionByCountries()) {
            $users = $this->filterByCountries($users, $restrictions->getCountriesCodes());
        }

        return array_values($users);
    }

    public function filterByActivationLength(array $users, int $activationLength): array
    {
        return array_filter(
            $users,
            fn(User $user) =>
                $user->getCreatedAt()->diff($user->getActivatedAt())->days >=
                $activationLength
        );
    }

    public function filterByCountries(array $users, CountryCodeCollection $countryCodeCollection): array
    {
        return array_filter(
            $users,
            fn(User $user) => $countryCodeCollection->contains($user->getCountryCode())
        );
    }
}
