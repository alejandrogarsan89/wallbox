<?php

declare(strict_types=1);

namespace App\Domain\User;

use App\Domain\Common\CountryCode;
use App\Domain\Common\Email;

final class User
{
    public function __construct(
        private int $id,
        private FirstName $firstName,
        private LastName $lastName,
        private Email $email,
        private CountryCode $countryCode,
        private \DateTimeImmutable $createdAt,
        private \DateTimeImmutable $activatedAt,
        private int $chargerId
    )
    {}

    public static function create(
        string $id,
        string $firstName,
        string $lastName,
        string $email,
        string $countryCode,
        string $createdAt,
        string $activatedAt,
        string $chargerId
    ) : self
    {
        return new self(
            (int) $id,
            new FirstName($firstName),
            new LastName($lastName),
            new Email($email),
            CountryCode::fromString($countryCode),
            new \DateTimeImmutable($createdAt),
            new \DateTimeImmutable($activatedAt),
            (int) $chargerId
        );
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFirstName(): FirstName
    {
        return $this->firstName;
    }

    public function getLastName(): LastName
    {
        return $this->lastName;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getCountryCode(): CountryCode
    {
        return $this->countryCode;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getActivatedAt(): \DateTimeImmutable
    {
        return $this->activatedAt;
    }

    public function getChargerId(): int
    {
        return $this->chargerId;
    }
}
