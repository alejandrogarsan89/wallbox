<?php

declare(strict_types=1);

namespace App\Domain\User;

interface UserSortStrategy
{
    public function compare(User $userA, User $userB): int;
}
