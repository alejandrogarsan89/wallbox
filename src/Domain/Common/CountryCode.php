<?php

declare(strict_types=1);

namespace App\Domain\Common;

use League\ISO3166\Guards;

final class CountryCode
{
    private function __construct(private string $code)
    {
        Guards::guardAgainstInvalidAlpha2($code);
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public static function fromString(string $countryCode): self
    {
        return new self(strtoupper($countryCode));
    }

    public function equals(CountryCode $countryCode)
    {
        return $this->code === $countryCode->getCode();
    }
}
