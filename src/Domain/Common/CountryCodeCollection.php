<?php

declare(strict_types=1);

namespace App\Domain\Common;

final class CountryCodeCollection
{
    private function __construct(private array $countryCodes = []) { }

    public function add(CountryCode $countryCode)
    {
        $this->countryCodes[] = $countryCode;
    }

    public function contains(CountryCode $needleCountryCode): bool
    {
        foreach ($this->countryCodes as $countryCode) {
            if ($needleCountryCode->equals($countryCode)) {
                return true;
            }
        }

        return false;
    }

    public static function fromValues(array $countryCodes): self
    {
        return new self(
            array_map(
                fn(string $countryCode) => CountryCode::fromString($countryCode),
                $countryCodes
            )
        );
    }
}
