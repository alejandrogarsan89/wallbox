<?php

declare(strict_types=1);

namespace App\Domain\Common;

final class Email
{
    public function __construct(private string $value)
    {
        $this->guardValidEmail($value);
    }

    public function getValue(): string
    {
        return $this->value;
    }

    private function guardValidEmail(string $value): void
    {
        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidEmailException("invalid email address");
        }
    }
}
