<?php

namespace App\Tests\Unit\Infrastructure\Repository\User;

use App\Domain\Common\CountryCodeCollection;
use App\Domain\User\DefaultUserSortStrategy;
use App\Domain\User\User;
use App\Domain\User\UserFilter;
use App\Domain\User\UserRestrictions;
use App\Domain\User\UserSort;
use App\Infrastructure\Csv\CsvReader;
use App\Infrastructure\Repository\User\CsvUserAdapter;
use App\Infrastructure\Repository\User\CsvUserRepository;
use PHPUnit\Framework\TestCase;

class CsvUserRepositoryTest extends TestCase
{
    private CsvUserRepository $csvUserRepository;

    public function setUp(): void
    {
        $this->csvUserRepository = new CsvUserRepository(
            new CsvReader(),
            new CsvUserAdapter(),
            new UserSort(new DefaultUserSortStrategy()),
            new UserFilter()
        );
    }

    public function testThatRepositoryReturnsAllUsers()
    {
        $users = $this->csvUserRepository->all(new UserRestrictions());
        $this->assertCount(6, $users);

        foreach ($users as $user) {
            $this->assertTrue($user instanceof User);
        }
    }

    public function testThatUsersAreFilteredByCountry()
    {
        $users = $this->csvUserRepository->all(new UserRestrictions(
            null,
            CountryCodeCollection::fromValues(['ES'])
        ));

        /** @var User $user */
        foreach ($users as $user) {
            $this->assertEquals('ES', $user->getCountryCode()->getCode());
        }
    }

    public function testThatUsersAreFilteredByActivationLength()
    {
        $users = $this->csvUserRepository->all(new UserRestrictions(
                2,
                null
        ));

        /** @var User $user */
        foreach ($users as $user) {
            $this->assertTrue($user->getActivatedAt()->diff($user->getCreatedAt())->days >= 2);
        }
    }

    public function testThatUsersAreFilteredByActivationLengthAndCountries()
    {
        $users = $this->csvUserRepository->all(new UserRestrictions(
            2,
            CountryCodeCollection::fromValues(['ES'])
        ));

        /** @var User $user */
        foreach ($users as $user) {
            $this->assertTrue($user->getActivatedAt()->diff($user->getCreatedAt())->days >= 2);
            $this->assertEquals('ES', $user->getCountryCode()->getCode());
        }
    }
}
