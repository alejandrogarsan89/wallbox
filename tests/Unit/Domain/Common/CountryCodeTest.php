<?php

namespace App\Tests\Unit\Domain\Common;

use App\Domain\Common\CountryCode;
use PHPUnit\Framework\TestCase;

class CountryCodeTest extends TestCase
{
    public function testValue()
    {
        $this->assertEquals('ES', (CountryCode::fromString('ES'))->getCode());
    }

    public function invalidCountryCodeProvider(): array
    {
        return [
            ['123'],
            ['ESA']
        ];
    }

    /**
     * @dataProvider invalidCountryCodeProvider
     * @param string $countryCode
     */
    public function testThatInvalidCountryCodeThrowsException(string $countryCode)
    {
        $this->expectException(\DomainException::class);
        CountryCode::fromString($countryCode);
    }

    public function testEquals()
    {
        $this->assertTrue(
            (CountryCode::fromString('ES'))
                ->equals(CountryCode::fromString('ES'))
        );

        $this->assertTrue(
            (CountryCode::fromString('es'))
                ->equals(CountryCode::fromString('ES'))
        );

        $this->assertFalse(
            (CountryCode::fromString('IT'))
                ->equals(CountryCode::fromString('ES'))
        );
    }
}
