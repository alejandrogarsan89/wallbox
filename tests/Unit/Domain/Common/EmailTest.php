<?php

namespace App\Tests\Unit\Domain\Common;

use App\Domain\Common\Email;
use App\Domain\Common\InvalidEmailException;
use PHPUnit\Framework\TestCase;

class EmailTest extends TestCase
{
    public function testThatInvalidEmailThrowsException()
    {
        $this->expectException(InvalidEmailException::class);
        new Email('gsdfgsdfgsdg');
    }
}
