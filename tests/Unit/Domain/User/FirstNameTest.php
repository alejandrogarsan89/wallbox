<?php

namespace App\Tests\Unit\Domain\User;

use App\Domain\User\EmptyValueException;
use App\Domain\User\FirstName;
use App\Domain\User\ValueExceedsAllowedLimitException;
use PHPUnit\Framework\TestCase;

class FirstNameTest extends TestCase
{
    public function testThatEmptyUsernameThrowsException()
    {
        $this->expectException(EmptyValueException::class);
        new FirstName('');
    }

    public function testThatUsernameThatExceedThresholdThrowsException()
    {
        $this->expectException(ValueExceedsAllowedLimitException::class);
        new FirstName('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
    }
}
