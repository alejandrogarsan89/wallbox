<?php

namespace App\Tests\Unit\Domain\User;

use App\Domain\User\DefaultUserSortStrategy;
use App\Domain\User\User;
use PHPUnit\Framework\TestCase;

class DefaultUserSortStrategyTest extends TestCase
{
    private DefaultUserSortStrategy $strategy;

    public function setUp(): void
    {
        $this->strategy = new DefaultUserSortStrategy();
    }

    public function testThatIsOrderedByFirstName()
    {
        $userA = User::create(
            id: 1,
            firstName: 'A',
            lastName: 'B',
            email: 'a@b.com',
            countryCode: 'ES',
            createdAt: '20200101',
            activatedAt: '20200102',
            chargerId: 2
        );

        $userB = User::create(
            id: 1,
            firstName: 'B',
            lastName: 'C',
            email: 'a@b.com',
            countryCode: 'ES',
            createdAt: '20200101',
            activatedAt: '20200102',
            chargerId: 2
        );

        $result = $this->strategy->compare($userA, $userB);
        $this->assertEquals(-1, $result);
        $result = $this->strategy->compare($userB, $userA);
        $this->assertEquals(1, $result);
    }

    public function testThatIsOrderedByLastNameIfFirstNamesAreEqual()
    {
        $userA = User::create(
            id: 1,
            firstName: 'A',
            lastName: 'B',
            email: 'a@b.com',
            countryCode: 'ES',
            createdAt: '20200101',
            activatedAt: '20200102',
            chargerId: 2
        );

        $userB = User::create(
            id: 1,
            firstName: 'A',
            lastName: 'C',
            email: 'a@b.com',
            countryCode: 'ES',
            createdAt: '20200101',
            activatedAt: '20200102',
            chargerId: 2
        );

        $result = $this->strategy->compare($userA, $userB);
        $this->assertEquals(-1, $result);

        $result = $this->strategy->compare($userB, $userA);
        $this->assertEquals(1, $result);
    }
}
