<?php

namespace App\Tests\Unit\Domain\User;

use App\Domain\Common\CountryCodeCollection;
use App\Domain\User\User;
use App\Domain\User\UserFilter;
use App\Domain\User\UserRestrictions;
use PHPUnit\Framework\TestCase;

class UserFilterTest extends TestCase
{
    public function testThatUsersAreFilteredByActivationLength()
    {
        $userA = User::create(
            id: 1,
            firstName: 'A',
            lastName: 'B',
            email: 'a@b.com',
            countryCode: 'ES',
            createdAt: '20200101',
            activatedAt: '20200102',
            chargerId: 2
        );

        $userB = User::create(
            id: 2,
            firstName: 'B',
            lastName: 'C',
            email: 'a@b.com',
            countryCode: 'ES',
            createdAt: '20200101',
            activatedAt: '20200103',
            chargerId: 2
        );

        $userRestrictions = new UserRestrictions(2);
        $userFilter = new UserFilter();
        $filteredUsers = $userFilter->filterByRestrictions([$userA, $userB], $userRestrictions);

        $this->assertCount(1, $filteredUsers);
        $this->assertEquals(2, $filteredUsers[0]->getId());

    }

    public function testThatUsersAreFilteredByCountries()
    {
        $userA = User::create(
            id: 1,
            firstName: 'A',
            lastName: 'B',
            email: 'a@b.com',
            countryCode: 'ES',
            createdAt: '20200101',
            activatedAt: '20200102',
            chargerId: 2
        );

        $userB = User::create(
            id: 2,
            firstName: 'B',
            lastName: 'C',
            email: 'a@b.com',
            countryCode: 'IT',
            createdAt: '20200101',
            activatedAt: '20200103',
            chargerId: 2
        );

        $userRestrictions = new UserRestrictions(null, CountryCodeCollection::fromValues(['ES']));
        $userFilter = new UserFilter();
        $filteredUsers = $userFilter->filterByRestrictions([$userA, $userB], $userRestrictions);

        $this->assertCount(1, $filteredUsers);
        $this->assertEquals(1, $filteredUsers[0]->getId());

    }
}
