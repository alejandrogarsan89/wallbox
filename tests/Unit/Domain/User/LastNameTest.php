<?php

namespace App\Tests\Unit\Domain\User;

use App\Domain\User\EmptyValueException;
use App\Domain\User\FirstName;
use App\Domain\User\LastName;
use App\Domain\User\ValueExceedsAllowedLimitException;
use PHPUnit\Framework\TestCase;

class LastNameTest extends TestCase
{
    public function testThatEmptyUsernameThrowsException()
    {
        $this->expectException(EmptyValueException::class);
        new LastName('');
    }

    public function testThatUsernameThatExceedThresholdThrowsException()
    {
        $this->expectException(ValueExceedsAllowedLimitException::class);
        new LastName('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
    }
}
