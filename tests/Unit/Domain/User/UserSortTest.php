<?php

namespace App\Tests\Unit\Domain\User;

use App\Domain\User\DefaultUserSortStrategy;
use App\Domain\User\User;
use App\Domain\User\UserSort;
use PHPUnit\Framework\TestCase;

class UserSortTest extends TestCase
{
    public function testThatUsersAreOrderedByFirstName()
    {
        $userOrder = new UserSort(new DefaultUserSortStrategy());
        $userA = User::create(
            id: 1,
            firstName: 'A',
            lastName: 'B',
            email: 'a@b.com',
            countryCode: 'ES',
            createdAt: '20200101',
            activatedAt: '20200102',
            chargerId: 2
        );

        $userB = User::create(
            id: 1,
            firstName: 'B',
            lastName: 'C',
            email: 'a@b.com',
            countryCode: 'ES',
            createdAt: '20200101',
            activatedAt: '20200102',
            chargerId: 2
        );

        $sortedUsers = $userOrder->sort([$userB, $userA]);
        $this->assertEquals('A', $sortedUsers[0]->getFirstName()->getValue());
    }
}
