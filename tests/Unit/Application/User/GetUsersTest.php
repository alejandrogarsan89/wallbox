<?php

namespace App\Tests\Unit\Application\User;

use App\Application\User\GetUsers\GetUsers;
use App\Application\User\GetUsers\GetUsersRequest;
use App\Domain\User\User;
use App\Domain\User\UserRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class GetUsersTest extends TestCase
{
    private GetUsers $getUsers;
    private UserRepository|MockObject $userRepository;

    public function setUp(): void
    {
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->getUsers = new GetUsers($this->userRepository);
    }

    public function testThatUsersAreReturned()
    {
        $this->userRepository->method('all')->willReturn([
            User::create(
                id: 1,
                firstName: 'alex',
                lastName: 'Garcia',
                email: 'a@b.com',
                countryCode: 'ES',
                createdAt: '20200101',
                activatedAt: '20200102',
                chargerId: 2
            )
        ]);

        $response = $this->getUsers->execute(new GetUsersRequest());

        $items = $response->jsonSerialize()['items'];

        $this->assertCount(1, $items);

        $this->assertArrayHasKey('id', $items[0]);
        $this->assertArrayHasKey('name', $items[0]);
        $this->assertArrayHasKey('surname', $items[0]);
        $this->assertArrayHasKey('email', $items[0]);
        $this->assertArrayHasKey('country', $items[0]);
        $this->assertArrayHasKey('createdAt', $items[0]);
        $this->assertArrayHasKey('activatedAt', $items[0]);
        $this->assertArrayHasKey('chargerId', $items[0]);

        $this->assertEquals(1, $items[0]['id']);
        $this->assertEquals('alex', $items[0]['name']);
        $this->assertEquals('Garcia', $items[0]['surname']);
        $this->assertEquals('a@b.com', $items[0]['email']);
        $this->assertEquals('ES', $items[0]['country']);
        $this->assertEquals('20200101', $items[0]['createdAt']);
        $this->assertEquals('20200102', $items[0]['activatedAt']);
        $this->assertEquals(2, $items[0]['chargerId']);
    }

    public function testThatInvalidCountriesThrowsException()
    {
        $this->expectException(\DomainException::class);
        $this->getUsers->execute(GetUsersRequest::fromRequest('', 'ES,REDF'));
    }

    public function testThatInvalidActivationLengthThrowsException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->getUsers->execute(GetUsersRequest::fromRequest('a', ''));
    }
}
