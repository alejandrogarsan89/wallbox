<?php

namespace App\Tests\Api\User;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class GetUsersControllerTest
 * https://github.com/symfony/monolog-bundle/issues/369
 * @package App\Tests\Api\User
 */
class GetUsersControllerTest extends WebTestCase
{
    public function testThatGetUsersReturnsOk()
    {
        $client = static::createClient();
        $client->request('GET', '/user');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testThatGetUsersReturnsExpectedUsers()
    {
        $client = static::createClient();
        $client->request('GET', '/user');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $items = json_decode($client->getResponse()->getContent(), true)['items'];
        $this->assertCount(6, $items);

        foreach ($items as $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('name', $item);
            $this->assertArrayHasKey('surname', $item);
            $this->assertArrayHasKey('email', $item);
            $this->assertArrayHasKey('country', $item);
            $this->assertArrayHasKey('createdAt', $item);
            $this->assertArrayHasKey('activatedAt', $item);
            $this->assertArrayHasKey('chargerId', $item);
        }

        $this->assertEquals(1, $items[0]['id']);
        $this->assertEquals('Alex', $items[0]['name']);
        $this->assertEquals('Garcia', $items[0]['surname']);
        $this->assertEquals('alejandrogarsan89@gmail.com', $items[0]['email']);
        $this->assertEquals('ES', $items[0]['country']);
        $this->assertEquals('20200101', $items[0]['createdAt']);
        $this->assertEquals('20200103', $items[0]['activatedAt']);
        $this->assertEquals('2', $items[0]['chargerId']);


    }

    public function testThatInvalidParameterCountriesReturnsBadRequest()
    {
        $client = static::createClient();
        $client->request('GET', '/user?countries=ES,123,UK');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testThatInvalidParameterActivationLengthReturnsBadRequest()
    {
        $client = static::createClient();
        $client->request('GET', '/user?activation_length=A');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function expectedUsersFilteredByCountryProvider(): array
    {
        return [
            ['ES', [1, 2, 6]],
            ['ES,IT', [1, 2, 4, 6]],
            ['FR', []],
            ['ZZ', []]
        ];
    }

    public function expectedUsersFilteredByActivationLengthProvider(): array
    {
        return [
            [0, [1, 2, 3, 4, 5, 6]],
            [2, [1, 3, 4, 6]],
            [9, [4]],
            [19, [4]],
            [20, []],
        ];
    }

    public function usersAreFilteredByActivationLengthAndCountryProvider(): array
    {
        return [
            ['ES', 6, [6]],
            ['ES,IT', 6, [6, 4]]
        ];
    }

    /**
     * @dataProvider usersAreFilteredByActivationLengthAndCountryProvider
     * @param string $countryCodes
     * @param int $activationLenght
     * @param array $expectedUserIds
     */
    public function testThatUsersAreFilteredByActivationLengthAndCountry(
        string $countryCodes,
        int $activationLenght,
        array $expectedUserIds
    ) {
        $client = static::createClient();
        $client->request('GET', "/user?countries=$countryCodes&activation_length=$activationLenght");

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $items = json_decode($client->getResponse()->getContent(), true)['items'];
        $responseUserIds = array_map(fn(array $item) => $item['id'], $items);

        $this->assertCount(count($expectedUserIds), $responseUserIds);

        foreach ($expectedUserIds as $expectedUserId) {
            $this->assertTrue(in_array($expectedUserId, $responseUserIds));
        }
    }

    /**
     * @dataProvider expectedUsersFilteredByActivationLengthProvider
     * @param array $expectedUserIds
     */
    public function testThatUsersAreFilteredByActivationLength(int $activationLenght, array $expectedUserIds)
    {
        $client = static::createClient();
        $client->request('GET', "/user?activation_length=$activationLenght");

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $items = json_decode($client->getResponse()->getContent(), true)['items'];
        $responseUserIds = array_map(fn(array $item) => $item['id'], $items);

        $this->assertCount(count($expectedUserIds), $responseUserIds);

        foreach ($expectedUserIds as $expectedUserId) {
            $this->assertTrue(in_array($expectedUserId, $responseUserIds));
        }
    }

    /**
     * @dataProvider expectedUsersFilteredByCountryProvider
     * @param string $countryCodes
     * @param array $expectedUsers
     */
    public function testThatUsersAreFilteredByCountry(string $countryCodes, array $expectedUserIds)
    {
        $client = static::createClient();
        $client->request('GET', "/user?countries=$countryCodes");

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $items = json_decode($client->getResponse()->getContent(), true)['items'];
        $responseUserIds = array_map(fn(array $item) => $item['id'], $items);

        $this->assertCount(count($expectedUserIds), $responseUserIds);

        foreach ($expectedUserIds as $expectedUserId) {
            $this->assertTrue(in_array($expectedUserId, $responseUserIds));
        }
    }
}
